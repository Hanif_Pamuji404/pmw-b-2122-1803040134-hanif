import { Component, OnInit } from '@angular/core';

import { Recipe } from './recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.page.html',
  styleUrls: ['./recipes.page.scss'],
})
export class RecipesPage implements OnInit {
  recipes: Recipe[] = [
    {
      id: 'r1',
      title: 'Boba',
      imageUrl: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Ftravel.kompas.com%2Fread%2F2019%2F10%2F12%2F100000327%2Fmengenal-perbedaan-istilah-boba-bubble-tea-dan-pearl%3Fpage%3Dall&psig=AOvVaw2rhyJ4llxE5Q_xaMl569i2&ust=1635488421654000&source=images&cd=vfe&ved=0CAgQjRxqFwoTCOiogqa77PMCFQAAAAAdAAAAABAI',
      ingredients: ['Tepung Tapioka', 'Air', 'Gula Merah/Pewarna Makanan']
    }
    {
      id: 'r2',
      title: 'Donat',
      imageUrl: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.kompas.com%2Ffood%2Fread%2F2020%2F12%2F14%2F205657275%2Fcara-membuat-donat-kentang-tanpa-ragi-dan-tanpa-oven&psig=AOvVaw2zIqTjNbFBcHR-MzqIPM-n&ust=1635488792457000&source=images&cd=vfe&ved=0CAgQjRxqFwoTCODTldm87PMCFQAAAAAdAAAAABAD',
      ingredients: ['Tepung Terigu', 'Ragi Instan', 'Gula Pasir', 'Susu Bubuk']
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
