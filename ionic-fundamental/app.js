const namaInput = document.querySelector('#input-nama');
const pengeluaranInput = document.querySelector('#input-pengeluaran');
const cancelBtn = document.querySelector('#btn-cancel');
const confirmBtn = document.querySelector('#btn-confirm');
const expensesList = document.querySelector('#expenses-list');
const totalBiayaOutput = document.querySelector('#total-biaya');
const alertCtrl = document.querySelector('ion-alert-controller');

let totalBiaya = 0;

const hapus = () => {
    namaInput.value = '';
    pengeluaranInput.value = '';
}

confirmBtn.addEventListener('click', () => {
    const enteredNama = namaInput.value;
    const enteredPengeluaran = pengeluaranInput.value;

    if (
        enteredNama.trim().length <= 0 ||
        enteredPengeluaran <= 0 ||
        enteredPengeluaran.trim().length <= 0
    ) {
        alertCtrl
        .create({
        message: 'Please isikan nama and pengeluaran!', 
        header: 'Invalid inputs',
        button: ['Okay']
        })
        .then(alertElement => {
        alertElement.present();
        });
        return;
    }
    const newItem = document.createElement('ion-item');
    newItem.textContent = enteredNama + ': Rp. ' + enteredPengeluaran;

    expensesList.appendChild(newItem);

    totalBiaya += +enteredPengeluaran;
    totalBiayaOutput.textContent = totalBiaya;
    hapus();
});

cancelBtn.addEventListener('click', hapus);
