import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root',
})
export class PlacesService {
  private _places: Place[] = [
    new Place(
      'p1',
      'Manhattan Mansion',
      'In the heart of New York City.',
      'https://img.okezone.com/content/2021/09/08/406/2468326/tertarik-pasang-iklan-di-billboard-times-square-new-york-segini-tarifnya-mgdnRQlwd0.JPG',
      149.99,
      new Date('2022-01-01'),
      new Date('2022-12-31')
    ),
    new Place(
      'p2',
      'Ancol',
      'In Jakarta City.',
      'https://asset.kompas.com/crops/AA9YnvkX46kxsDBEIN7Z0Dy3Owk=/0x0:0x0/750x500/data/photo/2020/06/20/5eede1b96770c.jpeg',
      30.0,
      new Date('2022-01-01'),
      new Date('2022-12-31')
    ),
    new Place(
      'p3',
      'Pangandara Beach',
      'In Pangandaran.',

      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Sunset_-_Pangandaran_-_Indonesia.jpg/800px-Sunset_-_Pangandaran_-_Indonesia.jpg',
      25.0,
      new Date('2022-01-01'),
      new Date('2022-12-31')
    ),
  ];

  get places() {
    return [...this._places];
  }
  constructor() {}

  getPlace(id: string) {
    return { ...this._places.find((p) => p.id === id) };
  }
}
