// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: 'AIzaSyAOU-5BTT0OJuSufOuRzKrke9TwZfBo-zo',
    authDomain: 'fireionicfirestore-9dddb.firebaseapp.com',
    projectId: 'fireionicfirestore-9dddb',
    storageBucket: 'fireionicfirestore-9dddb.appspot.com',
    messagingSenderId: '185113115439',
    appId: '1:185113115439:web:8f029cd54b5836cc207fda',
    measurementId: 'G-EMCNSYXY1E',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
